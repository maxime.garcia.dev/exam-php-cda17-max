<?php
require 'classes/Character.php';

$character1 = new Character('Rambo', 70, 45);
echo $character1->getName();
echo $character1->getAttack();
echo $character1->getDefense();

$character2 = new Character('Bayonneta', 115, 100);
echo $character2->getName();
echo $character2->getAttack();
echo $character2->getDefense();