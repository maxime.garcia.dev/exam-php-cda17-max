<?php

function specialSum(array $elements): float {
    $sum = 0;
    if (sizeof($elements) % 2 == 0) {
        foreach ($elements as $key=>$element){
            if($key % 2 != 0){
                $sum = $sum + $element;
            }
        }
    }
    else {
        foreach ($elements as $key=>$element){
            if($key % 2 == 0){
                $sum = $sum + $element;
            }
        }
    }
    return $sum;
}

function factorielle(int $number): int {
    $fac = 1;
    for($i= 2; $i <= $number; $i++){
        $fac = $fac * $i;
    }
    return $fac;
}

function displayPrice(float $price): string
{
    return number_format($price, 2, ',', '&nbsp;');
}

function getHTPrice(float $priceTTC): float
{
    return round($priceTTC / 1.2, 2);
}