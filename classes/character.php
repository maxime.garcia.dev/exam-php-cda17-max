<?php
class Character
{

    protected $name;
    protected $attack;
    protected $defense;

    public function __construct($name, $attack, $defense)
    {

        $this->name = $name;
        $this->attack = $attack;
        $this->defense = $defense;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($newName)
    {
        $this->name = $newName;
    }

    public function getAttack()
    {
        return $this->attack;
    }

    public function setAttack($newAttack)
    {
        $this->name = $newAttack;
    }

    public function getDefense()
    {
        return $this->defense;
    }

    public function setDefense($newDefense)
    {
        $this->name = $newDefense;
    }
}