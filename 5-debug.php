<?php
include_once __DIR__ . '/1-fonctions.php';
$availableSizes = [
    'S',
    'M',
    'L',
    'XL',
];

$availableMaterials = [
    'wool'     => 'Laine',
    'cashmere' => 'Cachemire',
    'silk'     => 'Soie',
    'cotton'   => 'Coton',
];

$beanies = [
    [
        'name'      => 'Bonnet en laine',
        'price'     => 10.00,
        'sizes'     => [
            'S',
            'M',
            'L',
        ],
        'materials' => [
            'wool',
        ],
    ],
    [
        'name'      => 'Bonnet en laine bio',
        'price'     => 14.00,
        'sizes'     => [
            'M',
            'L',
            'XL',
        ],
        'materials' => [
            'wool',
        ],
    ],
    [
        'name'      => 'Bonnet en laine et cachemire',
        'price'     => 20.00,
        'sizes'     => [
            'S',
            'L',
            'XL',
        ],
        'materials' => [
            'wool',
            'cashmere',
        ],
    ],
    [
        'name'      => 'Bonnet arc-en-ciel',
        'price'     => 12.00,
        'sizes'     => [
            'S',
            'M',
            'XL',
        ],
        'materials' => [
            'silk',
            'cotton',
        ],
    ],
];

$size = '';
if (!empty($_GET['size'])) {
    $size = $_GET['size'];
    $beanies = array_filter($beanies, function (array $beanie) use ($size) {
        return in_array($size, $beanie['sizes']);
    });
}

$material = '';
if (!empty($_GET['material'])) {
    $beanies = array_filter($beanies, function (array $beanie) use ($material) {
        return is_array($material, $beanie['materials']);
    });
}

$minPrice = 2;
if (!empty($_GET['minPrice'])) {
    $minPrice = $_GET['minPrice'];
    $beanies = array_filter($beanies, function (array $beanie) use ($minPrice) {
        $beanie['price'] >= $minPrice;
    });
}

$maxPrice = 100;
if (!empty($_GET['maxPrice'])) {
    $maxPrice = $_GET['maxPrice'];
    $beanies = array_filter($beanies, function (array $beanie) use ($maxPrice) {
        return $beanie['price'] <= $maxPrice;
    });
}
?>

<form action="" method="GET" class="form-inline">
    <div class="form-group mb-2">
        <label for="size" class="sr-only">Taille</label>
        <select name="size" class="form-control" id="size">
            <option value="" <?= ($size == '') ? 'selected="selected"' : '' ?>></option>
            <?php
            foreach ($availableSizes as $availableSize) {
                ?>
                <option value="<?= $availableSize; ?>" <?= ($size == $availableSize) ? 'selected="selected"' : '' ?>>
                    <?= $availableSize; ?>
                </option>
                <?php
            }
            ?>
        </select>
    </div>
    <div class="form-group mb-2">
        <label for="material" class="sr-only">Matériaux</label>
        <select name="material" class="form-control" id="material">
            <option value="" <?= ($material == '') ? 'selected="selected"' : '' ?>></option>
            <?php
            foreach ($availableMaterials as $value => $label) {
                ?>
                <option value="<?= $value; ?>" <?= ($material == $value) ? 'selected="selected"' : '' ?>>
                    <?= $label; ?>
                </option>
                <?php
            }
            ?>
        </select>
    </div>
    <div class="form-group mb-2">
        <label for="minPrice" class="sr-only">Prix minimum</label>
        <input type="number" name="minPrice" class="form-control" id="minPrice" value="<?= $minPrice; ?>">
    </div>
    <div class="form-group mb-2">
        <label for="maxPrice" class="sr-only">Prix maximum</label>
        <input type="number" name="maxPrice" class="form-control" id="maxPrice" value="<?= $maxPrice; ?>">
    </div>
    <button type="submit" class="btn btn-primary mb-2">Filtrer</button>
</form>

<table class="table">
    <thead>
    <tr>
        <th>Index</th>
        <th>Nom</th>
        <th>Prix HT</th>
        <th>Prix</th>
        <th>Description</th>
        <th>Acheter</th>
    </tr>
    </thead>
    <tbody>
    <?php
    foreach ($beanies as $key => $beanie) {
        $price = $beanie['price'];
        ?>
        <tr>
            <td>
                <?php echo $key ?>
            </td>
            <td>
                <?php echo $beanie['name'] ?>
            </td>
            <td>
                <?php echo displayPrice(getHTPrice($price)) ?> €
            </td>
            <td>
                <?php echo displayPrice($price) ?> €
            </td>
            <td>
                <!-- Descritpion ? -->
            </td>
        </tr>
        <?php
    }
    ?>
    </tbody>
</table>