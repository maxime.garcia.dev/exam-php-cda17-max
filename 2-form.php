<form action="" method="POST">
    <?php
    foreach ($errorMessages as $errorMessage) {
        ?>
        <p class="alert alert-danger" role="alert">
            <?= $errorMessage; ?>
        </p>
        <?php
    }
    ?>

    <div class="form-group">
        <label for="email">
            Email
        </label>
        <input id="email" type="email" name="email" class="form-control">
    </div>
    <div class="form-group">
        <label for="text">
            Un texte
        </label>
        <input id="text" type="text" name="text" class="form-control">
    </div>
    <button type="submit" class="btn btn-success">Valider</button>
</form>
