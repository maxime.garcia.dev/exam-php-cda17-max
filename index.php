<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title></title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
</head>

<?php
session_start();
require_once __DIR__ . '/1-fonctions.php';


$errorMessages = [];

$array1 = [
    12.2,
    5,
    7,
    75,
    3,
    12
];

$array2 = [
    6 => 12.2,
    12 => 5,
    7 => 7,
    9 => 75,
    1 => 3,
    12
];

var_dump(specialSum($array1));
var_dump(specialSum($array2));

var_dump(factorielle(5));
var_dump(factorielle(12));



if (isset($_POST['email']) && isset($_POST['text'])){

    trim($_POST['email']);
    trim($_POST['text']);
    $email = $_POST['email'];
    $text = $_POST['text'];

    $var = filter_var($email, FILTER_VALIDATE_EMAIL);

    if (empty($email)) {
        $errorMessages[] = 'Veuillez entrer une adresse e-mail.';
    }

    elseif ($var == false) {
       $errorMessages[] = 'Veuillez entrer une adresse e-mail valide.';
    }

    if (empty($text)){
        $errorMessages[] = 'Veuillez entrer un texte';
    }
    if (empty($errorMessages)) {
        echo $email . '<br>';
        echo $text . '<br>';
        echo '<a href="2-superglobales.php?value=test">Lien vers l\'exercice suivant</a>';
    }
}

include __DIR__ . '/2-form.php';